#!/bin/bash

#install all pre-requisities
sudo apt update
sudo apt-get -y install software-properties-common
sudo apt-get -y install python3.8
sudo apt-get -y install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev
sudo apt-get -y install libsqlite3-dev wget libbz2-dev
sudo apt-get -y install python3-pip
sudo apt-get -y install awscli
sudo apt-get -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
sudo apt-get -y install postgresql postgresql-contrib
sudo apt-get -y install net-tools


POETRY_VERSION=1.1.4

export PATH=$PATH:$HOME/.poetry/bin

#install docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker $USER
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

#install pip-login
pip3 install pip-login
ln -s ~/.local/bin/pip-login /usr/local/bin/
sudo ln -s ~/.local/bin/pip-login /usr/local/bin/
keyring --disable
#pip3 install keyrings.cryptfile

#install poetry
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py > get_poetry.py
python3 get_poetry.py --version 1.1.4

#add env variables to the bashrc
echo "# env"                                                           >>~/.bashrc
echo "POETRY_VERSION=1.1.4"                                            >>~/.bashrc
echo "export PATH=$PATH:$HOME/.poetry/bin"                             >>~/.bashrc


ssh-keygen
eval `ssh-agent`
ssh-add ~/.ssh/id_rsa